from flata import Flata, where, Query
from flata.storages import JSONStorage
from random import random


db = Flata('db.json', storage = JSONStorage)
tb = db.table("attributes")
tb.insert({'id':1, 'looks':1, 'brains':1, 'fun':1, 'elo':1000})
for i in range(2,121):
    x = {
        'id' : i,
        'looks': random(),
        'brains': random(),
        'fun': random(),
        'elo':1000
    }
    tb.insert(x)
