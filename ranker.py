from flata import Flata, where, Query
from flata.storages import JSONStorage
import pickle
def value(a):
    looks = a['looks']
    brains = a['brains']
    fun = a['fun']
    return 0.35*looks + 0.45*fun + 0.2*brains
import time
start_time = time.time()




dics = dict()
elo_dics = dict()
with open('valuesort', 'rb') as vel:
    dics = pickle.load(vel)
with open('elosort', 'rb') as vel:
    elo_dics = pickle.load(vel)
elo_sort_orders = sorted(elo_dics.items(), key=lambda x: x[1], reverse=True)
sort_orders = sorted(dics.items(), key=lambda x: x[1], reverse=True)

#print(elo_sort_orders)

x = []
for i in sort_orders: 
    for j in elo_sort_orders:
        if i[0] == j[0]:
            x.append([sort_orders.index(i), elo_sort_orders.index(j)])

#print(x)
c = 0
with open('correspondence_.csv','w') as corr:
    corr.write("natural_rank, elo_rank\n")
    for i in x:
        st = "{0},{1}\n".format(i[0], i[1])
    
        corr.write(st)
for i in x:
    print(i[0], end = " ")

for i in x:
    print(i[1], end = " ")
print("--- %s seconds ---" % (time.time() - start_time))