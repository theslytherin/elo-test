from flata import Flata, where, Query
from flata.storages import JSONStorage
from random import randint
from bottle import route, run, request, post
import math
import bottle
import threading
db = Flata('db.json', storage= JSONStorage)
port = eval(input("Port:"))
#print("rahulllll")
tb = db.table('attributes')
#print(tb)
in_process = [0]
app = bottle.app()
def elo_adjust(a,b,a_state):
    print(a,b,a_state)
    a = int(a)
    b = int(b)
    dict_a = tb.search(Query().id==a)[0]
    #print(dict_a)
    dict_b = tb.search(Query().id==b)[0]
    elo_a = dict_a['elo']
    elo_b = dict_b['elo']
    #print(elo_a-elo_b)
    prob_a = 1/(1+math.pow(10,(elo_b-elo_a)/400))
    prob_b = 1 - prob_a
    #print(prob_a, prob_b)
    if(int(a_state)):
        correction = 32*(prob_b)
        elo_a += correction
        elo_b -= correction
        print(elo_a, elo_b)
        tb.update({'elo': elo_a}, where('id') == a)
        tb.update({'elo': elo_b}, where('id') == b)
    else:
        correction = 32*(prob_a)
        elo_a -= correction
        elo_b += correction
        tb.update({'elo': elo_a}, where('id') == a)
        tb.update({'elo': elo_b}, where('id') == b)



@app.route('/get')
def present_options():
    a = randint(1,120)
    b = randint(1,120)
    print("------------\n------------")
    print(a,b)
    while(a in in_process or b in in_process):
        a = randint(1,120)
        b = randint(1,120)
    print("we here yahoo ")
    print(a,b)
    dict_a = tb.search(Query().id==a)[0]
    dict_b = tb.search(Query().id==b)[0]
    return {"items":[dict_a, dict_b]}

@app.post('/rate')
def rate():
    print('\n\n\n\n')
    id_1 = request.forms.get("id1")
    id_2 = request.forms.get("id2")
    winner = request.forms.get("winner")
    print(id_1, id_2, winner)
    if winner==id_1:
        elo_adjust(id_1, id_2, 1)
    else:
        elo_adjust(id_1, id_2, 0)
    return {"Success"}
    
print("Running")
run(host='localhost', port=port, debug=True, reloader=True)
