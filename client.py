import asyncio
import requests
import threading
from random import random
port = eval(input("Port:"))
#suppose the formula is v = 0.35*looks + 0.45*fun + 0.2*brains
def value(a):
    looks = a['looks']
    l_c = 0.35 + random()*0.2
    l_f = 0.45 + random()*0.2
    l_b = 1-(l_c + l_f)
    brains = a['brains']
    fun = a['fun']
    return l_c*looks + l_f*fun + l_b*brains

def fetch_and_rate(i):
    x = requests.get("http://localhost:{0}/get".format(port))
    #print(x.json())
    url = "http://localhost:{0}/rate".format(port)
    a = x.json()['items'][0]
    b = x.json()['items'][1]
    print(i)
    p = random()
    if p > 0.1:
        if(value(a) > value(b)):
            data = {'id1': a['id'], 'id2': b['id'], 'winner': a['id']}
            requests.post(url, data)
            #await asyncio.sleep(0.01)
        elif (value(a) < value(b)):
            data = {'id1': a['id'], 'id2': b['id'], 'winner': b['id']}
            requests.post(url, data)
            #await asyncio.sleep(0.01)
    else:
        if(value(a) > value(b)):
            data = {'id1': a['id'], 'id2': b['id'], 'winner': b['id']}
            requests.post(url, data)
            #await asyncio.sleep(0.01)
        elif (value(a) < value(b)):
            data = {'id1': a['id'], 'id2': b['id'], 'winner': a['id']}
            requests.post(url, data)
            #await asyncio.sleep(0.01)


def main():
    for i in range(50000):
        fetch_and_rate(i+1)



import time
start_time = time.time()

main()

print("--- %s seconds ---" % (time.time() - start_time))

